#include "Rect.h"
#include <vector>
#include <iostream>

Rect::Rect(float x, float y, float length)
{
	color = new std::vector<float>();
	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	color->push_back(r);
	r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	color->push_back(r);
	r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	color->push_back(r);
	this->x = x;
	this->y = y;
	this->length = length;
	std::cout << "Rect created X:" << x << " Y: " << y;
	std::cout << " length: " << length << " color: [" << color->at(0) << "," << color->at(1) << "," << color->at(2) << "] \n";

}


Rect::~Rect()
{
}

float Rect::getX(){
	return this->x;

}

float Rect::getY(){
	return this->y;
}

float Rect::getLenght(){
	return this->length;
}

std::vector<float>* Rect::getColor(){
	return this->color;
}
