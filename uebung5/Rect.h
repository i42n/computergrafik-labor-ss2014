#pragma once
#include <vector>
class Rect
{

private:
	std::vector<float>* color;
	float x, y, length;	
	
public:
	float getX();
	float getY();
	float getLenght();
	std::vector<float>* getColor();
	Rect(float x, float y, float length);
	~Rect();
};

