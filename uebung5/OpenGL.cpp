#include <GLUT/glut.h>
#include "Rect.h"
#include <vector>
#include <stdlib.h>

#define SPACEBAR 32

static std::vector<Rect*> *rects = new std::vector<Rect*>();

static void addRect(float x, float y, float length, std::vector<float>* color){
	glBegin(GL_QUADS);
	glColor3d(color->at(0), color->at(1), color->at(2));
	glVertex3f(x-length/2, y-length/2, -10);
	glColor3d(color->at(0), color->at(1), color->at(2));
	glVertex3f(x+length/2, y-length/2, -10);
	glColor3d(color->at(0), color->at(1), color->at(2));
	glVertex3f(x+length/2, y+length/2, -10);
	glColor3d(color->at(0), color->at(1), color->at(2));
	glVertex3f(x-length/2, y+length/2, -10);
    
	glEnd();
}

/* GLUT callback Handlers */
static void resize(int width, int height) {
	const float ar = (float)width / (float)height;
    
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);
	//glOrtho(-ar, ar, -1.0, 1.0, 2.0,100.0);
    
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
}

static void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (Rect* rect : *rects) {
		addRect(rect->getX(), rect->getY(), rect->getLenght(), rect->getColor());
	}
	glutSwapBuffers();
}

/*
 Handle keybord input events
 */
static void key(unsigned char key, int x, int y) {
	switch (key) {
        case 27:
        case 'q':
            exit(0);
            break;
        case SPACEBAR:
            // add new rect at random position
            Rect* rect = new Rect((rand() % 10) - 5, (rand() % 10) - 5, 1);
            rects->push_back(rect);
            break;
	}
	glutPostRedisplay();
}

static void idle(void) {
	glutPostRedisplay();
}

/* Program entry point */
int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitWindowSize(800	, 800);
	glutInitWindowPosition(200, 10);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    
	glutCreateWindow("Quadrate");
    
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(key);
	glutIdleFunc(idle);
    
	glClearColor(0, 0, 0, 0);
    
	glutMainLoop();
    
	return 0;
}

